//
//  Product.h
//  CollectionViewTask
//
//  Created by Ahmed Elashker on 6/29/16.
//  Copyright © 2016 Raye7. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (nonatomic, strong) NSString *productDescription;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSDictionary *image;

@end
