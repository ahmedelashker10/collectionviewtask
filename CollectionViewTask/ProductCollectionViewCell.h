//
//  ProductCollectionViewCell.h
//  CollectionViewTask
//
//  Created by Ahmed Elashker on 6/29/16.
//  Copyright © 2016 Raye7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@end
