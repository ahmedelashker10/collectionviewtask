//
//  APIWrapper.h
//  CollectionViewTask
//
//  Created by Ahmed Elashker on 6/29/16.
//  Copyright © 2016 Raye7. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit.h>
#import "Product.h"

@interface APIWrapper : NSObject

@property (nonatomic, strong) NSArray<Product*> *products;

- (void)configureRestKit;

@end
