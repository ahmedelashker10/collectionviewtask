//
//  APIWrapper.m
//  CollectionViewTask
//
//  Created by Ahmed Elashker on 6/29/16.
//  Copyright © 2016 Raye7. All rights reserved.
//

#import "APIWrapper.h"
#import "Product.h"

@implementation APIWrapper

- (void)configureRestKit
{
    RKObjectMapping* articleMapping = [RKObjectMapping mappingForClass:[Product class]];
    [articleMapping addAttributeMappingsFromDictionary:@{
                                                         @"productDescription": @"productDescription",
                                                         @"image": @"image",
                                                         @"price": @"price"
                                                         }];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:articleMapping method:RKRequestMethodAny pathPattern:nil keyPath:nil statusCodes:nil];
    NSURL *url = [NSURL URLWithString:@"http://grapesnberries.getsandbox.com/products?count=10&from=1"];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        self.products = result.array;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"products" object:nil];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
    }];
    [operation start];
}

@end
