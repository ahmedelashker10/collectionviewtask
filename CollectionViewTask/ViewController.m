//
//  ViewController.m
//  CollectionViewTask
//
//  Created by Ahmed Elashker on 6/29/16.
//  Copyright © 2016 Raye7. All rights reserved.
//

#import "ViewController.h"
#import "Product.h"
#import "ProductCollectionViewCell.h"
#import "APIWrapper.h"
#import <UIImageView+WebCache.h>

@interface ViewController () <UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>

@property (nonatomic, strong) APIWrapper *wrapper;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotification) name:@"products" object:nil];
    
    _wrapper = [[APIWrapper alloc] init];
    [_wrapper configureRestKit];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleNotification {
    
    [_collectionView reloadData];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat width = [[[_wrapper.products objectAtIndex:indexPath.item].image valueForKey:@"width"] floatValue];
    CGFloat height = [[[_wrapper.products objectAtIndex:indexPath.item].image valueForKey:@"height"] floatValue];
    
    return CGSizeMake(width, height);
}

#pragma mark - Collection view data source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return _wrapper.products.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProductCell" forIndexPath:indexPath];
    Product *product = [_wrapper.products objectAtIndex:indexPath.row];
    
    [cell.imgView sd_setImageWithURL:[NSURL URLWithString:[product.image valueForKey:@"url"]]];
    [cell.lblPrice setText:[NSString stringWithFormat:@"$%@", product.price]];
    [cell.lblDescription setText:product.productDescription];
    
    [cell.lblDescription setTranslatesAutoresizingMaskIntoConstraints:NO];

    return cell;
}

#pragma mark - Collection view layout delegate
- (CGFloat)heightForPhotoAtIndexPath:(NSIndexPath *)indexpath withWidth:(CGFloat)width {
    
    return [[[_wrapper.products objectAtIndex:indexpath.item].image valueForKey:@"height"] floatValue];
}

- (CGFloat)heightForAnnotationAtIndexPath:(NSIndexPath *)indexpath withWidth:(CGFloat)width {
    
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithString:[_wrapper.products objectAtIndex:indexpath.item].productDescription];
    CGRect rect = [attrStr boundingRectWithSize:CGSizeMake(width, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    return rect.size.height;
}

@end
